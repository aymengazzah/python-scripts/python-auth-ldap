#!/usr/bin/env python3

import os
from flask import Flask, render_template, request, redirect, session, make_response, flash
from ldap3 import Server, Connection, Tls, ALL
import ssl
from bs4 import BeautifulSoup

# Configuration du contexte SSL
context = ssl.SSLContext(ssl.PROTOCOL_TLS_SERVER)
context.load_cert_chain('cert.pem', 'key.pem')

# Initialisation de l'application Flask
app = Flask(__name__, static_folder='templates')
app.secret_key = os.environ.get('SECRET_KEY', os.urandom(24))

# Configuration des paramètres
LDAP_SERVER = os.environ.get('LDAP_SERVER', 'localhost')
LDAP_PORT = int(os.environ.get('LDAP_PORT', 636))
LDAP_USE_SSL = os.environ.get('LDAP_USE_SSL', 'True').lower() == 'true'
LDAP_START_TLS = os.environ.get('LDAP_START_TLS', 'False').lower() == 'true'
LDAP_SSL_SKIP_VERIFY = os.environ.get('LDAP_SSL_SKIP_VERIFY', 'False').lower() == 'true'
LDAP_SEARCH_BASE_DN = os.environ.get('LDAP_SEARCH_BASE_DN', "ou=annuaire,dc=domain,dc=fr")

# Fonction d'authentification LDAP
def ldap_authenticate(uid, password):
    ldap_dn = f"uid={uid},{LDAP_SEARCH_BASE_DN}"
    try:
        server = Server(LDAP_SERVER, port=LDAP_PORT, use_ssl=LDAP_USE_SSL, get_info=ALL, tls=Tls(validate=ssl.CERT_NONE if LDAP_SSL_SKIP_VERIFY else ssl.CERT_OPTIONAL))
        conn = Connection(server, user=ldap_dn, password=password, auto_bind=True)
        return conn.bind()
    except Exception as e:
        app.logger.error(f'LDAP authentication failed for user {uid}: {e}')
        return False

# Fonction pour ajuster les chemins des ressources dans le HTML
def adjust_resource_paths(html_content, base_path):
    soup = BeautifulSoup(html_content, 'html.parser')
    for tag in soup.find_all(['link', 'script', 'img']):
        if tag.name == 'link' and tag.get('href'):
            if tag['href'].startswith('/'):
                tag['href'] = f'{base_path}{tag["href"][1:]}'  # Retirer le '/' initial
        if tag.name == 'script' and tag.get('src'):
            if tag['src'].startswith('/'):
                tag['src'] = f'{base_path}{tag["src"][1:]}'  # Retirer le '/' initial
    return str(soup)

# Page de login
@app.route('/', methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        uid = request.form['uid']
        password = request.form['password']
        remember_me = request.form.get('remember')

        try:
            if ldap_authenticate(uid, password):
                session['uid'] = uid
                session['password'] = password

                resp = make_response(redirect('/'))
                if remember_me:
                    resp.set_cookie('uid', uid, httponly=True, secure=True)
                    resp.set_cookie('password', password, httponly=True, secure=True)
                return resp
            else:
                flash("Authentification échouée. Veuillez vérifier vos identifiants.", "danger")
        except Exception as e:
            flash(str(e), "danger")
        return redirect('/')
    else:
        return render_template('login.html')

if __name__ == '__main__':
    print(f'Param LDAP_SERVER = {LDAP_SERVER}')
    print(f'Param LDAP_PORT = {LDAP_PORT}')
    print(f'Param LDAP_USE_SSL = {LDAP_USE_SSL}')
    print(f'Param LDAP_START_TLS = {LDAP_START_TLS}')
    print(f'Param LDAP_SSL_SKIP_VERIFY = {LDAP_SSL_SKIP_VERIFY}')
    print(f'Param LDAP_SEARCH_BASE_DN = {LDAP_SEARCH_BASE_DN}')

    app.run(host='0.0.0.0', port=int(os.environ.get('PORT', 5000)), debug=True, ssl_context=context)
